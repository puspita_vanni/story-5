from django.shortcuts import render, redirect
from .models import Member, Activity
from .forms import MemberForm, ActivityForm
from django.contrib import messages

# Create your views here.

def activity(request):
    if request.method == "POST":
        form_activ = ActivityForm(request.POST)
        form_member = MemberForm(request.POST)
        if form_activ.is_valid():
            form_activ.save()
            messages.success(request, (f"Kegiatan {request.POST['activity']} berhasil ditambahkan!"))
            return redirect('story_6:activity')
        elif form_member.is_valid():
            form_member.save()
            messages.success(request, (f"{request.POST['member']} berhasil bergabung!"))
            return redirect('story_6:activity')
        else:
            messages.warning(request, (f"Input kamu tidak sesuai!"))
            return redirect('story_6:activity')
    else:
        form_activ = ActivityForm()
        form_member = MemberForm()
        activities = Activity.objects.all()
        members = Member.objects.all()
        context = {
            'form_activ' : form_activ,
            'form_member' : form_member,
            'activities' : activities,
            'members' : members
        }
        return render(request, 'story_6/index.html', context)

def delete(request,pk):
    activity = Activity.objects.get(id=pk)
    activity.delete()
    messages.warning(request, (f"Kegiatan {activity} berhasil dihapus!"))
    print(activity)
    print(pk)
    return redirect('story_6:activity')

def deleteMember(request,pk):
    member = Member.objects.get(id=pk)
    member.delete()
    print(member)
    print(pk)
    messages.warning(request, (f"{member} berhasil dihapus!"))
    return redirect('story_6:activity')
