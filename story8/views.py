from django.shortcuts import render, redirect
import requests
from django.http import HttpResponse, JsonResponse
import json

# Create your views here.

def index(request):
    context = {
        'title':'Search Book'
    }
    return render(request, 'story8/buku.html', context)

#implement search
def search(request, query):
    api_url = "https://www.googleapis.com/books/v1/volumes?q=" + query
    
    ret = requests.get(api_url)
    data = ret.json()
    print(data.items)

    return JsonResponse(data)
