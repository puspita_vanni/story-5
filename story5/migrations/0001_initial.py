# Generated by Django 3.1.2 on 2020-10-14 06:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Matkul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=100)),
                ('dosen', models.CharField(max_length=100)),
                ('sks', models.PositiveIntegerField()),
                ('deskripsi', models.TextField()),
                ('semester', models.CharField(choices=[('Ganjil', 'Ganjil'), ('Genap', 'Genap')], default='Ganjil', max_length=6)),
                ('tahun', models.CharField(choices=[('2019/2020', '2019/2020'), ('2020/2021', '2020/2021'), ('2021/2022', '2021/2022'), ('2022/2023', '2022/2023')], default='2019/2020', max_length=9)),
                ('ruang_kelas', models.CharField(max_length=20)),
            ],
        ),
    ]
